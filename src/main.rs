use anyhow::Result;
use log::info;
use schemafy_lib::{Expander, Schema};
use std::path::Path;
use std::{ffi::OsStr, path::PathBuf};

fn generate_struct_from(path: &Path) -> Result<String> {
    let json = std::fs::read_to_string(path)?;
    let schema: Schema = serde_json::from_str(&json)?;
    let root_name = schema
        .title
        .clone()
        .unwrap_or_else(|| path.file_name().unwrap().to_str().unwrap().to_string());
    let path_str = path.to_str().unwrap();
    let mut expander = Expander::new(Some(&root_name), path_str, &schema);
    let x = expander.expand(&schema);

    Ok(x.to_string())
}

fn generate_struct_from_dir(dir: PathBuf) -> Result<String> {
    let mut buf = String::new();
    buf.push_str("use serde::{Deserialize, Serialize};\n\n");
    for e in std::fs::read_dir(&dir)? {
        let e = e?;
        if e.path().extension() != Some(OsStr::new("json")) {
            info!("skipping file: {}", e.path().display());
            continue;
        }

        info!("loading file: {}", e.path().display());

        let code = generate_struct_from(&e.path())?;
        buf.push_str(&code);
        buf.push('\n');
    }

    Ok(buf)
}

fn main() -> Result<()> {
    env_logger::init();

    let dir = match std::env::args().nth(1) {
        Some(dir) => PathBuf::from(dir),
        None => {
            panic!("usage: schemafy-cli <dir with json schemas>");
        }
    };

    let output = generate_struct_from_dir(dir)?;
    println!("{}", output);
    Ok(())
}

#[cfg(test)]
mod test {
    use std::path::PathBuf;

    use super::*;

    fn resources_test_dir() -> PathBuf {
        let root_dir = env!("CARGO_MANIFEST_DIR");
        let mut root_dir = PathBuf::from(root_dir);
        root_dir.push("resources");
        root_dir.push("test");

        root_dir
    }

    #[test]
    fn generate_struct_from_file() {
        let schema_dir = resources_test_dir();
        let output = generate_struct_from_dir(schema_dir).unwrap();

        assert!(output.starts_with("use serde::{Deserialize, Serialize};"));
        assert!(output.contains("# [derive (Clone , PartialEq , Debug , Deserialize , Serialize)]"));
        assert!(output.ends_with(
            "pub struct ChangeConfigurationRequest { pub key : String , pub value : String }\n"
        ));
    }
}
