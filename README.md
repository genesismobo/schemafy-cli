# schemafy-cli

schemafy-cli is a tool which generates Rust structs from JSON Schema Draft 4 files.
Output is dumped to standard output.

## Usage:

```sh
cargo install schemafy-cli
```

```sh
schemafy-cli <path-to-schema-dir> | tee my_types.rs
```

or if we want this stuff to be prettier

```sh
schemafy-cli <path-to-schema-dir> | rustfmt |  tee my_types.rs
```

Example:
```sh
schemafy-cli ./resources/test | rustfmt | tee ocpp_16.rs
```
